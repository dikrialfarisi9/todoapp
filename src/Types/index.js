export const FETCH_TODO_SUCCESS = '@FETCH_TODO_SUCCESS';
export const ADD_TODO = '@ADD_TODO';
export const UPDATE_TODO = '@UPDATE_TODO';
export const DELETE_TODO = '@DELETE_TODO';
export const SET_LOADING = '@SET_LOADING';
