/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import {getTodos, AddTodos, DeleteTodos, UpdateTodos} from '../Actions';

const Todos = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [selectedStatus, setSelectedStatus] = useState('ON_PROGRESS');
  const [button, setButton] = useState('Save');
  const [selectedUser, setSelectedUser] = useState({});

  const dispatch = useDispatch();
  const todo = useSelector(state => state.todos);
  const loading = useSelector(state => state.loading);

  useEffect(() => {
    dispatch(getTodos());
  }, []);

  const kirimData = () => {
    const dataObject = {
      title: title,
      description: description,
      status: selectedStatus,
    };

    if (button === 'Save') {
      dispatch(AddTodos(dataObject));
      setTitle('');
      setDescription('');
      dispatch(getTodos());
    } else if (button === 'Update') {
      dispatch(UpdateTodos(selectedUser.id, dataObject));
      setTitle('');
      setDescription('');
      dispatch(getTodos());
      setButton('Save');
    }
  };

  const selectTodo = item => {
    setButton('Update');
    setSelectedUser(item);
    setTitle(item.title);
    setDescription(item.description);
  };

  const cancel = () => {
    setDescription('');
    setTitle('');
    setButton('Save');
  };

  if (loading) {
    return <ActivityIndicator />;
  } else {
    return (
      <View style={styles.root}>
        <ScrollView>
          <View style={styles.headingContainer}>
            <Text style={styles.heading}>Redux Todo List</Text>
          </View>

          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              onChangeText={text => setTitle(text)}
              value={title}
              placeholder="Title"
              placeholderTextColor="white"
            />
            <TextInput
              style={styles.input}
              onChangeText={text => setDescription(text)}
              value={description}
              placeholder="Description"
              placeholderTextColor="white"
            />
          </View>
          <View style={styles.statusesContainer}>
            <TouchableOpacity
              onPress={() => setSelectedStatus('ON_PROGRESS')}
              style={[
                styles.statusButton,
                selectedStatus === 'ON_PROGRESS' && styles.statusButtonSelected,
              ]}>
              <Text
                style={[
                  styles.statusButtonText,
                  selectedStatus === 'ON_PROGRESS' &&
                    styles.statusButtonTextSelected,
                ]}>
                Progress
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => setSelectedStatus('DONE')}
              style={[
                styles.statusButton,
                selectedStatus === 'DONE' && styles.statusButtonSelected,
              ]}>
              <Text
                style={[
                  styles.statusButtonText,
                  selectedStatus === 'DONE' && styles.statusButtonTextSelected,
                ]}>
                Done
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{flexDirection: 'row', marginLeft: 'auto', marginTop: 20}}>
            <TouchableOpacity
              style={styles.saveButon}
              onPress={() => kirimData()}>
              <Text style={styles.buttonText}>Save</Text>
              <Icon name="save" size={20} color="white" />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.cancelButton}
              onPress={() => cancel()}>
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.content}>
            <Text style={styles.todoDateText}>Monday</Text>

            {todo.map(itemTodo => (
              <View style={styles.cardListContainer}>
                <View style={styles.todoCard}>
                  <View style={styles.todoTitleContainer}>
                    <View style={styles.todoActionContainer}>
                      <Text style={styles.todoTitle}>{itemTodo.title}</Text>
                      <TouchableOpacity style={styles.doneBadge}>
                        <Text style={styles.doneBadgeText}>
                          {itemTodo.status}
                        </Text>
                      </TouchableOpacity>
                    </View>

                    <View style={styles.todoActionContainer}>
                      <TouchableOpacity
                        style={styles.editButton}
                        onPress={() => selectTodo(itemTodo)}>
                        <Icon name="edit" size={20} color="white" />
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() => dispatch(DeleteTodos(itemTodo.id))}>
                        <Icon name="trash-2" size={20} color="white" />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.todoDescription}>{itemTodo.title}</Text>
                  </View>
                </View>
              </View>
            ))}
          </View>
        </ScrollView>
      </View>
    );
  }
};

export default Todos;
