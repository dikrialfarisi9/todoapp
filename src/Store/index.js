import AsyncStorage from '@react-native-async-storage/async-storage';
import {applyMiddleware, createStore} from 'redux';
import ReduxThunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';

import Reducers from '../Reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const configPersist = persistReducer(persistConfig, Reducers);

export const Store = createStore(
  configPersist,
  applyMiddleware(ReduxThunk, reduxLogger),
);
export const Persistor = persistStore(Store);
