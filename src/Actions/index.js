import axios from 'axios';
import {
  FETCH_TODO_SUCCESS,
  SET_LOADING,
  ADD_TODO,
  UPDATE_TODO,
  DELETE_TODO,
} from '../Types/index';

export const saveTodos = data => ({
  type: FETCH_TODO_SUCCESS,
  payload: data,
});

export const addTodos = data => ({
  type: ADD_TODO,
  payload: data,
});

export const updateTodos = data => ({
  type: UPDATE_TODO,
  payload: data,
});

export const deleteTodos = id => ({
  type: DELETE_TODO,
  payload: id,
});

export const setLoading = value => ({
  type: SET_LOADING,
  payload: value,
});

//fungsi untuk mendapatkan data todos

export const getTodos = () => {
  return async dispatch => {
    const resTodos = await axios.get('http://code.aldipee.com/api/v1/todos');
    if (resTodos.data.results.length > 0) {
      dispatch(setLoading(false));
      dispatch(saveTodos(resTodos.data.results));
    }
  };
};

export const AddTodos = data => {
  return async dispatch => {
    try {
      await axios
        .post('http://code.aldipee.com/api/v1/todos', data)
        .then(() => {
          console.log('Sukses');
        });
      dispatch(addTodos(data));
    } catch (err) {
      console.log(err);
    }
  };
};

export const UpdateTodos = (id, data) => {
  return async dispatch => {
    try {
      await axios
        .patch(`http://code.aldipee.com/api/v1/todos/${id}`, data)
        .then(() => {
          console.log('Sukses');
        });
      dispatch(updateTodos(data));
    } catch (err) {
      console.log(err);
    }
  };
};

export const DeleteTodos = id => {
  return async dispatch => {
    try {
      await axios.delete(`http://code.aldipee.com/api/v1/todos/${id}`);
      dispatch(deleteTodos(id));
    } catch (err) {
      console.log(err);
    }
  };
};
